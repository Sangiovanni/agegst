@extends('layouts.app')
@section('content')
  <h1>Home</h1>
  <div class="pure-g">
    
    <div class="pure-u-4-24"><p></p></div>
    <div class="pure-u-4-24">
        <a href="{{ route('addmodfamily') }}"><img src="{{ asset('styles/icons/file-add.ico') }}" class="icons" alt="Aggiungi scheda" title="Aggiungi scheda" /></a>
    </div>
    <div class="pure-u-4-24">
        <a href="{{ route('searchfamily') }}"><img src="{{ asset('styles/icons/file-search.ico') }}" class="icons" alt="Cerca scheda" title="Cerca scheda" /></a>
    </div>
    <div class="pure-u-4-24">
        <a href="#"><img src="{{ asset('styles/icons/box.ico') }}" class="icons" alt="Distribuisci" title="Distribuisci" /></a>
    </div>
    <div class="pure-u-4-24">
        <a href="#"><img src="{{ asset('styles/icons/logout.ico') }}" class="icons" alt="Logout" title="Logout" /></a>
    </div>
    
    
</div>
@endsection

