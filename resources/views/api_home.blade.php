@include('layouts.header')
<body>

    API:

    <h3>To GET the list of Italian cities:</h3>


    @component('components.api_description')

    @slot('description','Return the complete list of cities')

    @slot('baseurl', URL::current())

    @slot('url', '/city')

    @slot('exampleurl','/city')

    @endcomponent


    @component('components.api_description')

    @slot('description','Returns the city with the specified id')

    @slot('baseurl', URL::current())

    @slot('url', '/city/{id}')

    @slot('exampleurl','/city/24')

    @endcomponent


    @component('components.api_description')

    @slot('description','Returns the cities that begin with the specified string')

    @slot('baseurl', URL::current())

    @slot('url', '/citysearchfield/{start_str}')

    @slot('exampleurl','/citysearchfield/mes')

    @endcomponent



    @component('components.api_description')

    @slot('description','As above, but the maximum number of cities returned will be equal to limit')

    @slot('baseurl', URL::current())

    @slot('url', '/Citysearchfield/{start_str}/{limit}')

    @slot('exampleurl','/citysearchfield/an/5')

    @endcomponent

</body>
@include('layouts.footer')

