<div class="pure-menu pure-menu-horizontal">
    <ul class="pure-menu-list">
        <li class="pure-menu-item">
            <a href="{{ route('home') }}" class="pure-menu-link">Home</a>
        </li>
        <li class="pure-menu-item pure-menu-has-children pure-menu-allow-hover">
            <a href="#" id="menuLink1" class="pure-menu-link">Agea</a>
            <ul class="pure-menu-children">
                <li class="pure-menu-item">
                    <a href="{{ route('load') }}" class="pure-menu-link">Carico viveri</a>
                </li>
                <li class="pure-menu-item">
                    <a href="{{ route('reg_list') }}" class="pure-menu-link">Reg. di carico e scarico</a>
                </li>
                <li class="pure-menu-item">
                    <a href="{{ route('all6') }}" class="pure-menu-link">Dichiar. di Consegna</a>
                </li>                
            </ul>     
        </li>
        <li class="pure-menu-item">
            <a href="{{ route('searchfamily') }}" class="pure-menu-link">Cerca scheda</a>
        </li>
        <li class="pure-menu-item">
            <a href="{{ route('addmodfamily') }}" class="pure-menu-link">Agg. scheda</a>
        </li>
        <li class="pure-menu-item">
            <form id="search-menu-form"  method="get" action="{{ route('blocksheet') }}">
                <button type = "submit" class="pure-button">Distribuisci a </button>
                <input class="form-control mr-sm-2" id="menu-search-for-sheet_num" type="text" placeholder="numero scheda">
            </form>
        </li>
    </ul>
</div>


<script>
    var form = document.getElementById('search-menu-form');
    form.addEventListener("submit", function (e) {
        e.preventDefault();
        var value = parseInt(document.getElementById('menu-search-for-sheet_num').value);
        if (!isNaN(value) && value > 0) {
            var b_url = form.getAttribute('action');
            window.location.href = b_url + "/" + value;
        }
    }, true);


</script> 