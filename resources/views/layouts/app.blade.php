@include('layouts.header')
    <body>
        @include('layouts.navbar')
        <div class="container">
            @yield('content')
        </div>
    </body>
@include('layouts.footer')