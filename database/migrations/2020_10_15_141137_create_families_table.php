<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFamiliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('family', function (Blueprint $table) {
            $table->id('family_register_number');
            $table->char('address', 200)->nullable();
            $table->char('district', 200)->nullable();
            $table->integer('district_id'); 
            $table->char('phonenumber', 200)->nullable();
            $table->char('id_dist_rit', 1)->nullable();
            $table->enum('state', ['complete', 'incomplete', 'deleted']);
            $table->text('note')->nullable();
            $table->date('expirydate_isee')->nullable();
            $table->timestamp('insertDate')->nullable();
            $table->integer('num_indig')->nullable();
            $table->integer('stranieri')->nullable();
            $table->integer('senzafissadimora')->nullable();
            $table->integer('diversamenteabili')->nullable();
            $table->float('reddito', 7, 2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('family');
    }
}
