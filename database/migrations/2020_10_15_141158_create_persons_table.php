<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('person', function (Blueprint $table) {
            $table->id('id_person');
            $table->char('imagelink', 100)->nullable();
            $table->integer('vPosition')->nullable();
            $table->char('surname', 100)->nullable();
            $table->char('name', 100)->nullable();
            $table->char('fiscal_code', 100)->nullable();
            $table->date('born_date')->nullable();
            $table->enum('search_result', ['on','off']);
            $table->char('sex', 1)->nullable();
            $table->boolean('removed_person')->nullable();
            $table->bigInteger('family_register_number')->unsigned(); 
            $table->foreign('family_register_number')->references('family_register_number')->on('family');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('person');
    }
}
