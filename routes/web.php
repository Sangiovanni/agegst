<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */




Route::view('/', 'home')->name('home');

Route::view('/home', 'home')->name('home');

Route::view('ricerca-scheda', 'searchfamily')->name('searchfamily');

Route::get('nuova-scheda', function () {    //id !=0 view data from already registered family, id=0: new registration, new family.
    return view('addmodfamily');    //Same view for all CRUD operations 
})->name('addmodfamily');

Route::get('distribuisci/{id?}', function ($id=0) {
    return view('blocksheet');
})->name('blocksheet');




Route::get('carico-agea', function () {
    return view('load');
})->name('load');

Route::get('dichiarazione+di+consegna', function () {
    return view('all6');
})->name('all6');


Route::view('registro-carico-e-scarico', 'reg_list')->name('reg_list');

