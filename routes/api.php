<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
//use App\Http\Controllers\CityController;
use App\Models\CityModel;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return view('api_home');
});

//Routing for Cities
Route::get('city', [CityModel::class, 'city']);
Route::get('city/{id}', [CityModel::class, 'cityByID']);
Route::get('/citysearchfield/{start_str?}/{limit?}', [CityModel::class, 'citysearchfield'])->where('limit', '[0-9]+');
//Route::get('/citysearchfield/{start_str?}/{limit?}', 'CityController@citySearchfield')->where('limit', '[0-9]+');


