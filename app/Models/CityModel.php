<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use DB;

class CityModel extends Model {

    protected $table = "comuni";
    protected $primaryKey = "id";
    public $timestamps = false;

    public function city() {
        //DB::enableQueryLog();


        CityModel::get();

        //var_dump(DB::getQueryLog());

        return response()->json(CityModel::get(), 200);
    }

    public function cityByID($id) {
        // DB::enableQueryLog();

        $city = CityModel::find($id);
        
         //var_dump(DB::getQueryLog());
         
         
        if (is_null($city)) {
            return response()->json(["message" => "Record not found"], 404);
        }
        return response()->json($city, 200);
    }

    public function citysearchfield($start_str = "xxx", $limit = 10000) { //limit=10000 is equivalent to get all cities; no city starts with xxx. An empty set is returned.
        //dd($start_str." - ".$limit);
        return response()->json(
                        CityModel::query()->where('nomeComune', 'LIKE', $start_str . '%')->take($limit)->get(),
                        200
        );
    }

}
